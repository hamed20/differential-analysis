# Differential analysis script

###########################################
#  -This script was used in the molecular description of the Parkinson’s Disease (PD)
###########################################



#Please follow the steps as commented in the R script.
The folder contains the following files:

-diffAnalyze_PD_GIT.R : The R script which identifies the differentially expressed genes/miRNAs using three methods:

 1) ANOVA test  

 2) moderated t-test

 3) area under the curve of the receiver operator characteristics (AUC ROC)

Namely, genes/miRNAs that were classified as differentially expressed by at least two of the three methods were included in the final list of differentially expressed genes/miRNAs.


-SampleData.RDATA :  a sample dataset for testing the R script

